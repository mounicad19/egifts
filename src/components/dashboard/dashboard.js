import React, { Component } from 'react';
import './dashboard.scss';
import categoriesList from './ImagesInfo';
import all from '../../images/all.png'
import teens from '../../images/teens.png';
import men from '../../images/men.png'
import women from '../../images/women.jpg'
import kids from '../../images/kids.png';
import categories from './dropdownCategories';
import 'font-awesome/css/font-awesome.min.css';
import 'react-input-range/lib/css/index.css';
import _ from 'lodash';


class Dashboard extends Component {

    constructor(props) {
        super(props);

        this.state = {
            category: [],
            value3: 10,

            accessoriesCategory: categoriesList.accessories,
            dressesCategory: categoriesList.dresses,
            trendingCategory: categoriesList.trending,

            activeIndex: 0,
            itemsPerEachSlide: 10,
            updatedCategory: [],
            filteredList: []
        }
        this.trendingCategoryCopy = this.state.trendingCategory.slice();
        this.dressesCategoryCopy = this.state.dressesCategory.slice();
        this.accessoriesCategoryCopy = this.state.accessoriesCategory.slice();
        this.categorySelected = ''
    }

    componentDidMount() {
        this.setValue(null);
        this.onSelectCategory('Dresses')
    }

    loadItemsPerSlide = (index, filteredCategory) => {
        if (Array.isArray(filteredCategory) && filteredCategory.length) {
            var abc = filteredCategory;

            this.setValue(null);
            const data = abc.slice((index * this.state.itemsPerEachSlide), ((index + 1) * (this.state.itemsPerEachSlide)));

            data.forEach(function (val) {
                var foundIndex = abc.indexOf(val);
                if (foundIndex != -1) {
                    abc.splice(foundIndex, 1)
                }
            });
            console.log('abc', abc);
            this.setState({ filteredList: abc }, () => {
                console.log('this.state.filteredList', this.state.filteredList)
            });
            this.setState({ category: data, activeIndex: index }, () => {
                console.log('this.state.category===>,this.state.activeIndex ', this.state.category, this.state.activeIndex)
            });
            console.log('filteredList', this.state.filteredList);

            console.log('category', this.state.category)
        }
    }

    setValue = value => {
        this.setState(prevState => ({
            category: {
                ...prevState.category,
                value
            }
        }));
    };

    resetCategoryVal() {
        this.setState({ category: Dashboard.initialState })
    }

    storeCategorySelected(categorySelected) {
        return this.categorySelected = categorySelected
    }

    onClickType = (gender) => {
        var abc;
        var matchedArr;
        if (this.categorySelected) {
            if (this.categorySelected === 'Trending') {
                if (this.trendingCategoryCopy) { this.trendingCategoryCopy = this.state.trendingCategory.slice() }
                abc = this.trendingCategoryCopy;
            } else if (this.categorySelected === 'Dresses') {
                if (this.dressesCategoryCopy) { this.dressesCategoryCopy = this.state.dressesCategory.slice() }
                abc = this.dressesCategoryCopy;
            } else if (this.categorySelected === 'Accessories') {
                if (this.accessoriesCategoryCopy) { this.accessoriesCategoryCopy = this.state.accessoriesCategory.slice() }
                abc = this.accessoriesCategoryCopy;
            }
            if (gender === 'All') {
                matchedArr = abc
            } else {
                matchedArr = abc.filter(data => data.type === gender);
            }
            if (Array.isArray(matchedArr) && matchedArr.length) {
                this.setState({
                    category: matchedArr
                })
                console.log('matchedArr', matchedArr)
                this.loadItemsPerSlide(this.state.activeIndex, matchedArr)
            } else {
                this.setState({
                    category: null
                });
                console.log('matchedArr', matchedArr)
                this.loadItemsPerSlide(this.state.activeIndex, null)
            }
        }

    }

    onSelectCategory = (categoryVal) => {
        this.setValue(null);
        this.storeCategorySelected(categoryVal);

        this.state.category = {
            All: [],
            Dresses: [],
            Accessories: []
        }
        var abc;

        if (categoryVal === 'Trending') {
            abc = this.trendingCategoryCopy
        } else if (categoryVal === 'Dresses') {
            abc = this.dressesCategoryCopy
        } else if (categoryVal === 'Accessories') {
            abc = this.accessoriesCategoryCopy
        }

        const filteredCategory = abc.filter(data => data.category === categoryVal);
        if (Array.isArray(filteredCategory) && filteredCategory.length) {
            this.setState({
                category: filteredCategory
            })
            console.log('filteredCategory', filteredCategory)
            this.loadItemsPerSlide(this.state.activeIndex, filteredCategory)
        } else {
            this.setState({
                category: null
            });
            console.log('filteredCategory', null)
            this.loadItemsPerSlide(this.state.activeIndex, null)
        }
    }

    gotoNextSlide = (e) => {
        e.preventDefault();
        let index = this.state.activeIndex;
        var slides;
        if (this.state.filteredList.length < 10) {
            slides = 1;
        } else {
            slides = Math.ceil((this.state.filteredList.length) / this.state.itemsPerEachSlide);
        }
        let slidesLen = slides - 1;
        if (index === slidesLen) {
            index = -1
        }
        ++index;

        this.setState({ activeIndex: index }, () => {
            console.log('this.state.activeIndex', this.state.activeIndex);
        });
        this.loadItemsPerSlide(this.state.activeIndex, this.state.filteredList);
    }

    gotoPreviousSlide = (e) => {
        e.preventDefault();
        let index = this.state.activeIndex;
        var selectedCategory;
        if (this.categorySelected) {
            if (this.categorySelected === 'Dresses') {
                if (this.dressesCategoryCopy.length > 0) {
                    selectedCategory = this.dressesCategoryCopy
                } else {
                    this.dressesCategoryCopy = this.state.dressesCategory.slice();
                    selectedCategory = this.dressesCategoryCopy
                }
            } else if (this.categorySelected === 'Trending') {

                if (this.trendingCategoryCopy.length > 0) {
                    selectedCategory = this.trendingCategoryCopy
                } else {
                    this.trendingCategoryCopy = this.state.trendingCategory.slice();
                    selectedCategory = this.trendingCategoryCopy
                }
            } else if (this.categorySelected === 'Accessories') {
                if (this.accessoriesCategoryCopy.length > 0) {
                    selectedCategory = this.accessoriesCategoryCopy
                } else {
                    this.accessoriesCategoryCopy = this.state.accessoriesCategory.slice();
                    selectedCategory = this.accessoriesCategoryCopy
                }
            }

            let slides = Math.ceil((selectedCategory.length) / this.state.itemsPerEachSlide)
            let slidesLen = slides - 1;
            if (index < 1) {
                index = slidesLen
            }

            --index;

            this.setState({ activeIndex: index }, () => {
                console.log('this.state.activeIndex', this.state.activeIndex);
            });
            this.loadItemsPerSlide(this.state.activeIndex, selectedCategory);
        }
    }

    filterByPrice(priceVal) {
        const maxPrice = '$'.concat(priceVal)
        var categoryArr;
        if (this.categorySelected) {
            if (this.categorySelected === 'Trending') {
                if (this.trendingCategoryCopy) { this.trendingCategoryCopy = this.state.trendingCategory.slice() }
                categoryArr = this.trendingCategoryCopy;
            } else if (this.categorySelected === 'Dresses') {
                if (this.dressesCategoryCopy) { this.dressesCategoryCopy = this.state.dressesCategory.slice() }
                categoryArr = this.dressesCategoryCopy;
            } else if (this.categorySelected === 'Accessories') {
                if (this.accessoriesCategoryCopy) { this.accessoriesCategoryCopy = this.state.accessoriesCategory.slice() }
                categoryArr = this.accessoriesCategoryCopy;
            }
        }
        // var minprice='$25'
        const matchedPriceFilter = categoryArr.filter((data) => data.price <= maxPrice);
        var sortedPriceFilter = _.orderBy(matchedPriceFilter, 'price', 'asc')
        if (Array.isArray(sortedPriceFilter) && sortedPriceFilter.length) {
            this.setState({
                category: sortedPriceFilter
            })
            console.log('sortedPriceFilter', sortedPriceFilter)
            this.loadItemsPerSlide(this.state.activeIndex, sortedPriceFilter)
        } else {
            this.setState({
                category: null
            });
            console.log('sortedPriceFilter', sortedPriceFilter)
            this.loadItemsPerSlide(this.state.activeIndex, null)
        }
    }

    onInput = (e) => {
        this.setState({
            [e.target.id]: e.target.value
        })
        this.filterByPrice(e.target.value)
    }

    render() {
        const { category, activeIndex } = this.state;
        // const sliceVar = category.slice((activeIndex * this.state.itemsPerEachSlide) + 1, ((activeIndex + 1) * (this.state.itemsPerEachSlide)) + 1)

        return (
            <div className="dashboardLayout">
                <div class="filtersContainer">
                    <div class="textCss"> Christmas gifts</div>
                    <div className="dropdownCss">
                        <div className="f-10"> SELECT CATEGORY</div>
                        <select onChange={(e) => this.onSelectCategory(e.target.value)}>
                            {
                                categories.map(data => {
                                    const { value, label } = data;
                                    return <option key={label} value={value}>{label}</option>
                                })
                            }
                        </select>
                    </div>

                    <div className="horizontal-line"></div>

                    <div className="genderCss">
                        <div className="f-10"> GIFT FOR</div>
                        <div className="alignGenderProps">
                            <a className="widthCss"> <img src={all} className="genderDimensions" onClick={() => this.onClickType('All')} />
                                <p className="f-9 m-t-0 m-b-0">All</p>
                            </a>
                            <a className="widthCss"> <img src={men} className="genderDimensions" onClick={() => this.onClickType('Male')} />
                                <p className="f-9 m-t-0 m-b-0">Men</p>
                            </a>
                            <a className="widthCss"> <img src={women} className="womanIconDimensions" onClick={() => this.onClickType('Female')} />
                                <p className="f-9 m-t-0 m-b-0">Women</p>
                            </a>
                            <a className="widthCss"> <img src={kids} className="genderDimensions" onClick={() => this.onClickType('Kids')} />
                                <p className="f-9 m-t-0 m-b-0" style={{ marginLeft: 5 }}>Kids</p>
                            </a>
                            <a className="widthCss"> <img src={teens} className="genderDimensions" onClick={() => this.onClickType('Teens')} />
                                <p className="f-9 m-t-0 m-b-0">Teens</p>
                            </a>
                        </div>
                    </div>
                    <div className="input-slider-css">
                        <p className="set-price">SET PRICE</p>
                        <input id="typeinp" className="slider-width" type="range" min="25" max="1500" defaultValue="1000" step="1" onChange={this.onInput} />
                        <p className="font-input-range"> &nbsp;$25 &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp; $350 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp; $600 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp; $1000 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp; $1000+</p>
                    </div>
                </div>

                <div class="carousel">

                    {this.state.activeIndex === 0 ? null :
                        <a
                            href="#"
                            className="carousel__arrow carousel__arrow--left"
                            onClick={e => this.gotoPreviousSlide(e)}
                        >
                            <span className="fa fa-2x fa-angle-left" />
                        </a>
                    }
                    <div className=" middleDiv">
                        <div class="slideContainer">
                            {category ?

                                category.map((data, index) =>
                                    <div class="imageCss">
                                        <div className="imageBackground zoom">
                                            <img className="imageProps " src={data.imagepath} />
                                            <div className="imageDetailsProps m-l-5">{data.name}</div>
                                            <div className="carousel-slide__content m-l-5">{data.price}</div>
                                        </div>
                                    </div>
                                )

                                : 'no items to display'
                            }
                        </div>
                    </div>

                    <a
                        href="#"
                        className="carousel__arrow carousel__arrow--right"
                        onClick={e => this.gotoNextSlide(e)}
                    >
                        <span className="fa fa-2x fa-angle-right" />
                    </a>
                </div>

            </div >
        );
    }
}

export default Dashboard