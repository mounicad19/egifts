
import bag from '../../images/bag.jpg';
import bag2 from '../../images/bag2.jpg';
import bag3 from '../../images/bag3.jpg';

import dress1 from '../../images/dress1.jpg';
import dress2 from '../../images/dress2.jpg'
import dress3 from '../../images/dress3.jpg';
import dress4 from '../../images/dress4.jpg';
import dress5 from '../../images/dress5.jpg';
import dress6 from '../../images/dress6.jpg';

import toy from '../../images/toy.jpg';
import toy6 from '../../images/toy6.jpg';
import shoes1 from '../../images/shoes1.jpg';
import shoes2 from '../../images/shoes2.jpg';
import shoes3 from '../../images/shoes3.jpg';
import lipstick1 from '../../images/lipstick1.jpg';


const categoriesList = {
    trending: [

        {
            "imagepath": toy,
            "name": "amazon",
            "price": "$2.5",
            "category": "Trending",
            "type": "Kids"
        },
        {
            "imagepath": toy6,
            "name": "amazon",
            "price": "$22.5",
            "category": "Trending",
            "type": "Kids"
        },
        {
            "imagepath": lipstick1,
            "name": "amazon",
            "price": "$22.5",
            "category": "Trending",
            "type": "Teens"
        },
        {
            "imagepath": lipstick1,
            "name": "amazon",
            "price": "$22.5",
            "category": "Trending",
            "type": "Teens"
        },
        {
            "imagepath": shoes2,
            "name": "amazon",
            "price": "$22.5",
            "category": "Trending",
            "type": "Male"
        },
        {
            "imagepath": lipstick1,
            "name": "amazon",
            "price": "$22.5",
            "category": "Trending",
            "type": "Teens"
        },
        {
            "imagepath": lipstick1,
            "name": "amazon",
            "price": "$22.5",
            "category": "Trending",
            "type": "Teens"
        },
        {
            "imagepath": shoes1,
            "name": "amazon",
            "price": "$22.5",
            "category": "Trending",
            "type": "Male"
        },
        {
            "imagepath": shoes1,
            "name": "amazon",
            "price": "$22.5",
            "category": "Trending",
            "type": "Male"
        },
        {
            "imagepath": shoes1,
            "name": "amazon",
            "price": "$22.5",
            "category": "Trending",
            "type": "Male"
        },
        {
            "imagepath": shoes2,
            "name": "amazon",
            "price": "$22.5",
            "category": "Trending",
            "type": "Male"
        },
        {
            "imagepath": shoes1,
            "name": "amazon",
            "price": "$22.5",
            "category": "Trending",
            "type": "Male"
        },
        {
            "imagepath": shoes2,
            "name": "amazon",
            "price": "$22.5",
            "category": "Trending",
            "type": "Male"
        }
    ],
    dresses: [
        {
            "imagepath": dress1,
            "name": "amazon",
            "price": "$90.5",
            "category": "Dresses",
            "type": "Female"
        },
        {
            "imagepath": dress2,
            "name": "amazon",
            "price": "$222.5",
            "category": "Dresses",
            "type": "Teens"
        },
        {
            "imagepath": dress3,
            "name": "amazon",
            "price": "$110.5",
            "category": "Dresses",
            "type": "Female"
        },
        {
            "imagepath": dress2,
            "name": "amazon",
            "price": "$600.5",
            "category": "Dresses",
            "type": "Teens"
        },
        {
            "imagepath": dress4,
            "name": "amazon",
            "price": "$450.5",
            "category": "Dresses",
            "type": "Female"
        },
        {
            "imagepath": dress3,
            "name": "amazon",
            "price": "$220.5",
            "category": "Dresses",
            "type": "Teens"
        },
        {
            "imagepath": dress4,
            "name": "amazon",
            "price": "$100.5",
            "category": "Dresses",
            "type": "Female"
        },
        {
            "imagepath": dress2,
            "name": "amazon",
            "price": "$22.5",
            "category": "Dresses",
            "type": "Teens"
        },
        {
            "imagepath": dress1,
            "name": "amazon",
            "price": "$500.5",
            "category": "Dresses",
            "type": "Female"
        },
        {
            "imagepath": dress4,
            "name": "amazon",
            "price": "$400",
            "category": "Dresses",
            "type": "Teens"
        },
        {
            "imagepath": dress1,
            "name": "amazon",
            "price": "$225",
            "category": "Dresses",
            "type": "Teens"
        },
    ]
    ,
    accessories: [
        {
            "imagepath": bag,
            "name": "amazon",
            "price": "$2.5",
            "category": "Accessories",
            "type": "Female"
        },
        {
            "imagepath": bag,
            "name": "amazon",
            "price": "$12.5",
            "category": "Accessories",
            "type": "Teens"
        },
        {
            "imagepath": bag,
            "name": "amazon",
            "price": "$22.5",
            "category": "Accessories",
            "type": "Teens"
        },
        {
            "imagepath": bag,
            "name": "amazon",
            "price": "$22.5",
            "category": "Accessories",
            "type": "Teens"
        },
        {
            "imagepath": bag,
            "name": "amazon",
            "price": "$45.5",
            "category": "Accessories",
            "type": "Teens"
        },
        {
            "imagepath": bag3,
            "name": "amazon",
            "price": "$22.5",
            "category": "Accessories",
            "type": "Teens"
        },
        {
            "imagepath": bag2,
            "name": "amazon",
            "price": "$90.5",
            "category": "Accessories",
            "type": "Female"
        },
    ]

}
export default categoriesList;

